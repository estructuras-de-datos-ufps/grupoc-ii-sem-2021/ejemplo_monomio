/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Semestre;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class SIA {

    private Semestre[] semestres = new Semestre[10];

    public SIA() {
    }

    public SIA(String url) {

        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        //Leer el archivo: 
        Object datos[] = archivo.leerArchivo();
        //i=1 , por que la cabecera NO se procesa
        //codigos;nombre de estudiante;email;semestre
        this.crearSemestre();
        for (int i = 1; i < datos.length; i++) {
            String datoEstudiante = datos[i].toString();
            String datos2[] = datoEstudiante.split(";");
            int cod = Integer.parseInt(datos2[0]);
            String nombre = datos2[1];
            String email = datos2[2];
            short semestre = Short.parseShort(datos2[3]);
            //Esto para coincidir con el índice del vector
            semestre--;
            //Donde insertar ese estudiante?
            this.semestres[semestre].insertarEstudiante(cod, nombre, email);
            //System.out.println(fila.toString());
        }
    }

    /**
     * Método para crear los semestres en blanco
     */
    private void crearSemestre() {
        for (int i = 0; i < this.semestres.length; i++) {
            this.semestres[i] = new Semestre();
        }
    }

    @Override
    public String toString() {
        String msg = "";
        for (int i = 0; i < this.semestres.length; i++) {
            if (this.semestres[i].contieneEstudiante()) {
                msg += "\n \n Semestre:" + (i + 1) + ":\n" + this.semestres[i].toString();
            } else {
                msg += "No contiene estudiantes el semestre:" + (i + 1);
            }
        }
        return msg;
    }

    /**
     * Obtiene el(los) semestres con la mayor cantidad de estudiantes
     * @return un String del tipo: semestre-1,semestre-2...
     */
    public String getSemestre_Mas_Estudiantes()
    {
        return "";
    }
    
}
