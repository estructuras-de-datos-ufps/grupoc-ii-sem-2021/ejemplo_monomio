/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import java.util.Iterator;
import java.util.ListIterator;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class Test_ListaCD {

    public static void main(String[] args) {
        ListaCD<Integer> l = new ListaCD();
        for (int i = 0; i < 10; i++) {
            l.insertarInicio(i);
            
        }
        System.out.println("MI lista es:\n"+l.toString());
        
        System.out.println("Operando con iteradores:");
        
        //1. Forma de recorrer:
        System.out.println("Operando con foreach");
        for(int dato:l)
            System.out.println(dato+"<->");
        
        //2. Forma de recorrer: 
        
        Iterator<Integer> it=l.iterator();
        System.out.println("Operando con iterator");
        while(it.hasNext())
            System.out.println(it.next());
        //De forma inversa:
        System.out.println("Operando con last_iterator");
        ListIterator<Integer> inverso=l.last_iterator();
        while(inverso.hasPrevious())
            System.out.println(inverso.previous());
    }
    
    
    
    
}
