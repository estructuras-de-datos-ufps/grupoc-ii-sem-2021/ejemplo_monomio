/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class Test_ListaS_Iterador {

    public static void main(String[] args) {
        int n = 400000;
        experimento_Lista_1(n);
        experimento_Lista_2(n);
    }

    /**
     * Experimento 1, usando get
     */
    private static void experimento_Lista_1(int n) {
        ListaS<Integer> l = new ListaS();
        
        for (int i = 1; i <= n; i++) {
            l.insertarInicio(i * 7);
        }
        int s = 0;
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < l.getTamanio(); i++) {
            s += l.get(i);
        }
        System.out.println("La suma es:" + s);
        long total_tiempo = (System.currentTimeMillis() - startTime);
        System.out.println("Tiempo:" + (float)(total_tiempo/1000)+" seg");
    }

    private static void experimento_Lista_2(int n) {
        ListaS<Integer> l = new ListaS();
        for (int i = 1; i <= n; i++) {
            l.insertarInicio(i * 7);
        }
        int s = 0;
        long startTime = System.currentTimeMillis();

        for (Integer dato : l) {
            s += dato;
        }

        System.out.println("La suma es:" + s);
        long total_tiempo = (System.currentTimeMillis() - startTime);
        System.out.println("Tiempo:" + (float)(total_tiempo/1000)+" seg");
    }

}
