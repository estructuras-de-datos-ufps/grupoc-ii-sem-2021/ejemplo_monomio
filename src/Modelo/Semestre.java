/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class Semestre {

    private ListaCD<Estudiante> estudiantes;

    public Semestre() {
        this.estudiantes = new ListaCD();
    }

    public ListaCD<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void insertarEstudiante(int codigo, String nombre, String email) {
        this.estudiantes.insertarFin(new Estudiante(codigo, nombre, email));
    }

    @Override
    public String toString() {
        String msg = "";
        for (Estudiante x : this.estudiantes) {
            msg += "\n" + x.toString();
        }
        return msg;
    }
    
    
    public boolean contieneEstudiante()
    {
        return !this.estudiantes.esVacia();
    }
    
    public int getCantidadEstudiantes()
    {
        return this.estudiantes.getTamanio();
    }

}
