/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;

/**
 *
 * @author madar
 */
public class IteratorLCD<T> implements Iterator<T> {

    private NodoD<T> inicio;
    private NodoD<T> cabezaAux;

    IteratorLCD(NodoD<T> inicio) {
        this.cabezaAux = inicio;
        this.inicio = this.cabezaAux.getSig();
    }

    @Override
    public boolean hasNext() {
        return this.inicio != this.cabezaAux;
    }

    @Override
    public T next() {
        if (this.inicio == this.cabezaAux) {
            throw new RuntimeException("No hay más posiciones");
        }

        this.inicio = this.inicio.getSig();
        return inicio.getAnt().getInfo();
    }

}
