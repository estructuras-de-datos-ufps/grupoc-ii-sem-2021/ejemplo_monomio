/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.ListIterator;

/**
 * Recorrer la lista de forma inversa
 *
 * @author madarme
 */
public class LastIterator_ListaCD<T> implements ListIterator<T> {

    private NodoD<T> inicio;
    private NodoD<T> cabezaAux;

    public LastIterator_ListaCD(NodoD<T> inicio) {
        this.cabezaAux = inicio;
        this.inicio = inicio.getAnt();
    }

    @Override
    public boolean hasPrevious() {
        return this.inicio != this.cabezaAux;
    }

    @Override
    public T previous() {
        if (this.inicio == this.cabezaAux) {
            throw new RuntimeException("No hay más posiciones");
        }

        this.inicio = this.inicio.getAnt();
        return inicio.getSig().getInfo();
    }

    /**
     * ************************************
     */
    @Override
    public T next() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasNext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int nextIndex() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int previousIndex() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void set(T e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(T e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
